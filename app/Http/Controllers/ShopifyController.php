<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\ShopifyService;

class ShopifyController extends Controller
{
    private $shopifyService;

    // Constructor to inject the ShopifyService
    public function __construct(ShopifyService $shopifyService)
    {
        $this->shopifyService = $shopifyService;
    }

    // Method to get the shop name from the ShopifyService
    public function getShopName()
    {
        $shopName = $this->shopifyService->getShopName();

        return response()->json(['shopName' => $shopName]);
    }

    // Method to get the product count from the ShopifyService
    public function getProductCount()
    {
        $count = $this->shopifyService->getProductCount();

        return response()->json(['count' => $count]);
    }

    // Method to get a single product by its ID from the ShopifyService
    public function getSingleProduct($id)
    {
        $product = $this->shopifyService->getSingleProduct($id);

        return response()->json($product);
    }

    // Method to get multiple products from the ShopifyService
    public function getMultipleProducts()
    {
        $products = $this->shopifyService->getMultipleProducts();

        return response()->json($products);
    }

    // Method to get a product by a query from the ShopifyService
    public function getInput(Request $request)
    {
        return $this->shopifyService->getProductByQuery($request);
    }

    public function getMultiProducts(Request $request)
    {
        return $this->shopifyService->getMultiProducts($request);
    }

    // Method to create a new product in Shopify using the provided request data
    public function addSingleProduct(Request $request)
    {
        return $this->shopifyService->addSingleProduct($request);
    }

    // Method to create a multiple new product in Shopify using the provided request data
    public function addMultipleProducts(Request $request)
    {
        return $this->shopifyService->addMultipleProducts($request);
    }

    public function updatePrice(Request $request)
    {
        $validated = $request->validate([
            'sku' => 'required|string',
            'price' => 'required|numeric',
        ]);

        $sku = $validated['sku'];
        $newPrice = $validated['price'];

        $updateResult = $this->shopifyService->updateVariantPriceBySKU($sku, $newPrice);

        if (isset($updateResult['error'])) {
            return response()->json(['error' => $updateResult['error']], 400);
        }

        return response()->json(['message' => 'Variant price updated successfully', 'data' => $updateResult], 200);
    }
}

?>