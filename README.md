# Shopify API Development

## Prerequisites

Before starting the setup process, ensure you have the following prerequisites:

1.	**Shopify Account and Store**
    - **Description**: You need an active Shopify account and a store where you will be developing and testing the API.
    - Steps:
        - Sign up for a Shopify account at [Shopify](https://www.shopify.com/).
        - Create a store by following the on-screen instructions.

2.	**Development Environment**
    - **Operating System:** Windows, macOS, or Linux.
    - **Description:** Ensure your development environment is set up with the necessary software and tools.

3.	**Software and Tools**
    - **XAMPP:** A complete web development package including Apache, PHP, and MySQL.
        - **Installation:** Download XAMPP from [apachefriends.org](https://www.apachefriends.org/) (latest version).
        - Follow the installation instructions for your operating system.

    - **Composer:** Dependency manager for PHP.
        - **Installation:** 
            - **Windows:** Download and install from [getcomposer.org](https://getcomposer.org/).
            - **macOS/Linux:** Run the following command:
            ```
            curl -sS https://getcomposer.org/installer | php sudo mv composer.phar /usr/local/bin/composer
            ```

    - **Laravel: PHP framework.**
        - **Installation:**
            - PHP (7.x and above)
            - Run the following command on cmd:
            ```
            composer global require laravel/installer
            ```
            - Add Laravel to your system PATH.
                - Open the Start menu, search for "Environment Variables", and select "Edit the system environment variables".
                - In the System Properties window, click on the "Environment Variables" button.
                - In the Environment Variables window, find the "Path" variable in the "System variables" section and select it, then click "Edit".
                - Click "New" and add the path to Composer's global bin directory (e.g., 
                                      `C:\Users\<YourUsername>\AppData\Roaming\Composer\vendor\bin`
                ).
                - Click "OK" to close all windows.
    - **Postman:** API development tool.
        - Installation:
        - Download and install from [postman.com](https://www.postman.com/).

4.	**Database**
    - **MySQL or MariaDB:** A database management system for storing data (included in XAMPP).
    - **Configuration:**
        - XAMPP includes MySQL, so ensure it's running by starting the XAMPP Control Panel.

5. **Text Editor or IDE**
    - **Recommendation:** Visual Studio Code, PHPStorm, or any other text editor/IDE you are comfortable with.
    - **Installation:**
        - Download and install [Visual Studio Code](https://code.visualstudio.com/).

# Step-by-Step Setup
## **Step 1: Setting Up Your Shopify Store**
1.	**Create a Shopify Account:**
    - Go to [Shopify](https://www.shopify.com/) and sign up for an account.
    - Follow the on-screen instructions to set up your store.
2.	**Create a Custom App in Shopify:**
    - Navigate to the **Apps** section in your Shopify admin.
    - Click **App and Sales Channel Settings**.
    - Click **Develop Apps**.
    - Click Allow **Custom App Developmen**t and proceed to **App Development**
    - Click **Create an App** and name your app
    - Click **Configure Admin API scopes** and check the boxes with the following (It should be 99 selected API scopes):
        - _Analytics, Assigned Fulfillment Orders (2 ), Browsing Behavior, Customers (2), Discounts (2), Discovery (2), Draft Orders (2), Files (2), Fulfillment Services (2), Gift Cards (2), Inventory (2), Legal Policies (2), Locations (2), Marketing events (2), Merchant-managed fulfillment orders (2), Metaobject definitions (2), Metaobject entries (2), Online Store navigation (2), Online Store pages (2), Order editing (2), Orders (2), Packing slip management (2), Payment customizations (2), Payment terms (2), Pixels (2), Price rules (2), Product feeds (2), Product listings (2), Products (2), Publications (2), Purchase options (2), Reports (2), Resource feedback (2), Returns (2), Sales channels (2), Script tags (2), Shipping (2), Shop locales (2), Shopify Markets (2), Shopify Payments accounts, Shopify Payments bank accounts, Shopify Payments disputes (2nd  box only), Shopify Payments payouts, Store content (2), Themes (2), Third-party fulfillment orders (2), Translations (2), all_cart_transforms, cart_transforms (2), custom_fulfillment_services (2), delivery_customizations (2), fulfillment_constraint_rules (2),  gates (2)._
 
    - Go back to configuration tab and click **configure** of **Store API Integration** to check the following box:
        - **Product:** Read products, variants, and collections (1st box only)
 

    - Click **Install app** to generate **API credentials** (Admin API access token, Storefront API access token, API key, and API secret key) and save it.
 
    - Don’t forget to save the store **URL** which will be used for connecting the store to Laravel.

## Step 2: Setting Up XAMPP
1.	**Install and Configure XAMPP:**
    - Download XAMPP from [apachefriends.org](https://www.apachefriends.org/).
    - Follow the installation instructions specific to your operating system.
o	Open the XAMPP Control Panel and start Apache and MySQL.
 
2.	**Set Up a Local Development Environment:**
    - Create a new folder for your project in the XAMPP `htdocs` directory (e.g., `C:\xampp\htdocs\shopify-api`)
    
## Step 3: Setting Up Laravel
1.	**Install Laravel:**
    - Open a terminal and download the Laravel installer using Composer:
    ```
    composer global require laravel/installer
    ```
 
    - Run the following command to create a new Laravel project:
    ```
    composer create-project --prefer-dist laravel/Laravel yourproject
    ```
 
2.	**Configure Environment Variables:**
    - Open the `.env` file in your project directory.
    - Set up your database connection:
    ```
    DB_CONNECTION=mysql
    DB_HOST=127.0.0.1
    DB_PORT=3306
    DB_DATABASE=your_database_name
    DB_USERNAME=your_database_user
    DB_PASSWORD=your_database_password
    ```

    - **Add your Shopify API credentials:**
    ```
    SHOPIFY_SHOP_URL=https://your_url
    SHOPIFY_ACCESS_TOKEN=your_admin_access_token
    ```

3.	Run Migrations (optional):
    - Run the following command to set up your database:
`php artisan migrate`

4.	**Install Necessary Libraries:**
    - Install GuzzleHTTP for making HTTP requests:

    ```
    composer require guzzlehttp/guzzle
    ```

    - Install PHPish for interacting with the Shopify API (you can use other libraries for interacting Shopify API and this is one of them):
    ```
    composer require phpish/shopify
    ```

5.	**Develop and Test API:**
    1.	Create the ShopifyService Class:
        - Create a new file `app/Services/ShopifyService.php` class for handling Shopify API interactions .

6.  **Create a Controller for Shopify API:**
    - Generate a new controller:

    ```
    php artisan make:controller ShopifyController
    ```

    - Implement methods in `ShopifyController` to interact with the `ShopifyService`.

7.	**Create Routes for API**:
    - Add route in `routes/api.php`

## Step 4: Testing with Postman
1.	Import API Collection:
    - Open Postman and create a new collection.
    - Define the endpoints you created in Laravel (use the localhost of Laravel and the API route that you created).
 
2.	Test API Endpoints:
    - Send requests to your API endpoints to ensure they work as expected.
    - Use Postman’s testing features to automate some of your tests.

By following these steps, you will have a fully functional API development environment using Shopify, Laravel, Postman, and XAMPP. This setup will allow you to develop, test, and refine your APIs efficiently, ensuring seamless integration and communication between your online shop and its backend systems.

### References:
Shopify Documentation:
https://shopify.dev/docs/api 

Laravel Installation:
https://laravel.com/docs/11.x/installation

Github(libraries):
https://github.com/phpish/shopify/tree/master







