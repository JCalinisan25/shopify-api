<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ShopifyController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/shopify/shop', [ShopifyController::class, 'getShopName']);
Route::get('/shopify/count', [ShopifyController::class, 'getProductCount']);
Route::get('/shopify/single/{id}', [ShopifyController::class, 'getSingleProduct']);
Route::get('/shopify/multi', [ShopifyController::class, 'getMultipleProducts']);
Route::get('/input', [ShopifyController::class, 'getInput']);
Route::post('/multi', [ShopifyController::class, 'getMultiProducts']);
Route::post('/add', [ShopifyController::class, 'addSingleProduct']);
Route::post('/mul', [ShopifyController::class, 'addMultipleProducts']);
Route::post('/shopify/variant/update-price', [ShopifyController::class, 'updatePrice']);