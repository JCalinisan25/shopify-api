<?php

namespace App\Services;

use GuzzleHttp\Client;
use Illuminate\Http\Request;

class ShopifyService
{   
    // the HTTP client that will be used to make requests to the Shopify API
    private $client;

    //initializes the HTTP client
    public function __construct()
    {
        $this->client = $this->getShopifyClient();
    }

    // Method that creates and returns a new HTTP client configured for the Shopify API
    private function getShopifyClient()
    {
        return new Client([
            'base_uri' => env('SHOPIFY_SHOP_URL') . '/admin/api/2024-04/',
            'headers' => [
                'X-Shopify-Access-Token' => env('SHOPIFY_ACCESS_TOKEN'),
                'Content-Type' => 'application/json',
            ],
        ]);
    }

    // function for getting the shop name of the shopify
    public function getShopName()
    {
        $response = $this->client->post('graphql.json', [
            'json' => [
                'query' => '
                    {
                        shop {
                            name
                        }
                    }
                ',
            ],
        ]);

        $data = json_decode($response->getBody()->getContents(), true);

        if (!isset($data['data']['shop']['name'])) {
            // Handle the error here. You can return a default value or throw an exception.
            return null; // return a default value
        }

        return $data['data']['shop']['name'];
    }

    // Product Count function
    public function getProductCount()
    {
        $response = $this->client->post('graphql.json', [
            'json' => [
                'query' => '
                    {
                        products(first: 10) {
                            pageInfo {
                                hasNextPage
                            }
                            edges {
                                cursor
                            }
                        }
                    }
                ',
            ],
        ]);

        $data = json_decode($response->getBody()->getContents(), true);

        if (!isset($data['data']['products']['edges'])) {
            // Handle the error here. You can return a default value or throw an exception.
            return 0; // return a default value
        }

        return count($data['data']['products']['edges']);
    }

    // Single Product function
    public function getSingleProduct($id)
    {
        $formattedId = 'gid://shopify/Product/' . $id;

        $response = $this->client->post('graphql.json', [
            'json' => [
                'query' => '
                    {
                        product(id: "' . $formattedId . '") {
                            id
                            title
                            handle
                            description
                            tags
                        }
                    }
                ',
            ],
        ]);

        $data = json_decode($response->getBody()->getContents(), true);

        if (!isset($data['data']['product'])) {
            // Handle the error here. You can return a default value or throw an exception.
            return null; // return a default value
        }

        return $data['data']['product'];
    }

    // Multiple Products function
    public function getMultipleProducts()
    {
        $response = $this->client->post('graphql.json', [
            'json' => [
                'query' => '
                    {
                    products(first: 10) {
                        edges {
                        node {
                            handle
                            id
                            description
                            title
                            tags
                        }
                    }
                }
            }
            ',
            ],
        ]);

        return json_decode($response->getBody()->getContents(), true);
    }

    public function getProductByQuery(Request $request)
    {
        // Retrieve the 'handle' query parameter from the request
        $handle = $request->query('handle');

        // error handling for handle and return as an error response
        if (!$handle) {
            return response()->json(['error' => 'Missing handle query parameter'], 400);
        }

        // Make a POST request to the Shopify GraphQL API and Query a product by its handle
        $response = $this->client->post('graphql.json', [
            'json' => [
                'query' => '
                    {
                        productByHandle(handle: "' . $handle . '") {
                            id
                            title
                            description
                            tags
                            
                        }
                    }
                ',
            ],
        ]);

        // Decode the JSON response from the Shopify API into an associative array
        $data = json_decode($response->getBody()->getContents(), true);
        
        // error handling for productByHandle and data array, it return with error message
        if (!isset($data['data']['productByHandle'])) {
            return response()->json(['error' => 'Product not found'], 404);
        }

        // return as a JSON response
        return response()->json($data['data']['productByHandle']);
    }

    public function getMultiProducts(Request $request)
    {
        // Retrieve the JSON payload from the request body
        $data = $request->json()->all();

        // If the data is a single object, wrap it in an array
        // if (isset($data['handle'])) {
        //     $data = [$data];
        // }

        // If the data is a single object, return an error
        if (isset($data['handle'])) {
            return response()->json(['error' => 'Multiple products expected, but only one provided'], 400);
        }

        $responses = [];

        foreach ($data as $productData) {
            // Retrieve the 'handle' from the product data
            $handle = $productData['handle'];

            // Make a POST request to the Shopify GraphQL API and Query a product by its handle
            $response = $this->client->post('graphql.json', [
                'json' => [
                    'query' => '
                        {
                            productByHandle(handle: "' . $handle . '") {
                                id
                                title
                                description
                                tags
                            }
                        }
                    ',
                ],
            ]);

            // Decode the JSON response from the Shopify API into an associative array
            $responseData = json_decode($response->getBody()->getContents(), true);

            // If the product was not found, return an error response
            if (!isset($responseData['data']['productByHandle'])) {
                $responses[] = ['error' => 'Product not found'];
            } else {
                // Add the found product to the responses array
                $responses[] = $responseData['data']['productByHandle'];
            }
        }

        return $responses;
    }

    public function addSingleProduct(Request $request)
    {
        // Retrieve the JSON payload from the request body
        $productData = $request->json()->all();

        // Define the required fields
        $requiredFields = [
            'title', 
            'body_html', 
            'vendor', 
            'tags', 
            'product_type',
            'variants.0.sku',
            'variants.0.price'
        ];
        
        // Initialize a string to store missing fields
        $missingFields = '';

        // Check if all necessary product details are provided
        foreach ($requiredFields as $field) {
            if (!data_get($productData, $field)) {
                // Check if the field is a variant field
                if (strpos($field, 'variants') !== false) {
                    // Adjust the field name for the error message
                    $fieldName = str_replace('variants.0.', '', $field);
                    $missingFields .= $fieldName . ' in the first variant, ';
                } else {
                    $missingFields .= $field . ', ';
                }
            }
        }

        // If there are missing fields, return an error response
        if (!empty($missingFields)) {
            // Remove the trailing comma and space
            $missingFields = rtrim($missingFields, ', ');
            return ['error' => 'Missing: ' . $missingFields];
        }

        // Define the product details
        $productDetails = [
            'product' => [
                'title' => $productData['title'],
                'body_html' => $productData['body_html'],
                'vendor' => $productData['vendor'],
                'product_type' => $productData['product_type'],
                'tags' => $productData['tags'],
                'variants' => [
                    [
                        'price' => $productData['variants'][0]['price'],
                        'sku' => $productData['variants'][0]['sku'],
                        'inventory_management' => 'shopify',
                        'inventory_quantity' => 10
                    ]
                ]
                // Add more product details as needed
            ],
        ];

        try {
            // Make a POST request to the Shopify API to create a new product
            $response = $this->client->post('products.json', [
                'json' => $productDetails,
            ]);

            // Decode the JSON response into an associative array
            $responseData = json_decode($response->getBody()->getContents(), true);

            // If the product was not created successfully, return an error response
            if (!isset($responseData['product'])) {
                return ['error' => 'Failed to create product'];
            } else {
                // Return the created product
                return $responseData['product'];
            }
        } catch (\Exception $e) {
            // Handle the exception
            return ['error' => $e->getMessage()];
        }
    }

    public function addMultipleProducts(Request $request)
    {
        // Retrieve the JSON payload from the request body
        $productsData = $request->json()->all();

        // Check if the products data is empty
        if (empty($productsData)) {
            return ['error' => 'Failed to create product'];
        }

        // Initialize an array to store the responses
        $responses = [];

        // Loop through each product data
        foreach ($productsData as $productData) {
            // Create a new request for each product
            $productRequest = new Request();
            $productRequest->json()->replace($productData);

            // Call the addSingleProduct function and store the response
            $response = $this->addSingleProduct($productRequest);

            // If there's an error in the response, return immediately
            if (isset($response['error'])) {
                return $response;
            }

            $responses[] = $response;
        }

        // Return the responses
        return $responses;
    }

    public function updateVariantPriceBySKU($sku, $newPrice)
    {
            // GraphQL query to find variant ID by SKU
            $query = <<<GRAPHQL
            {
                products(first: 1, query: "sku:$sku") {
                    edges {
                        node {
                            variants(first: 1) {
                                edges {
                                    node {
                                        id
                                    }
                                }
                            }
                        }
                    }
                }
            }
            GRAPHQL;
    
            // Make GraphQL request to Shopify to find the variant ID
            $response = $this->client->post('graphql.json', [
                    'json' => [
                            'query' => $query,
                    ],
            ]);
    
            // Decode JSON response
            $responseData = json_decode($response->getBody()->getContents(), true);
    
            // Check for errors or unexpected response structure
            if (!isset($responseData['data']['products']['edges'][0]['node']['variants']['edges'][0]['node']['id'])) {
                    return ['error' => 'Variant not found with the given SKU'];
            }
    
            // Extract variant ID
            $variantId = $responseData['data']['products']['edges'][0]['node']['variants']['edges'][0]['node']['id'];
    
            // Prepare mutation input for updating variant price
            $mutationVariables = [
                    'input' => [
                            'id' => $variantId,
                            'price' => $newPrice,
                    ],
            ];
    
            // GraphQL mutation to update variant price
            $updateQuery = <<<GRAPHQL
            mutation variantUpdate(\$input: ProductVariantInput!) {
                productVariantUpdate(input: \$input) {
                    productVariant {
                        id
                        price
                    }
                    userErrors {
                        field
                        message
                    }
                }
            }
            GRAPHQL;
    
            // Make GraphQL request to update variant price
            $response = $this->client->post('graphql.json', [
                    'json' => [
                            'query' => $updateQuery,
                            'variables' => $mutationVariables,
                    ],
            ]);
    
            // Decode JSON response
            $updateResponseData = json_decode($response->getBody()->getContents(), true);
    
            // Check for errors in update response
            if (!isset($updateResponseData['data']['productVariantUpdate'])) {
                    return ['error' => 'Variant update failed. Check your update data and try again.'];
            }
    
            // Check for user errors
            if (!empty($updateResponseData['data']['productVariantUpdate']['userErrors'])) {
                    return ['error' => $updateResponseData['data']['productVariantUpdate']['userErrors'][0]['message']];
            }
    
            // Return updated variant data
            return $updateResponseData['data']['productVariantUpdate']['productVariant'];
    }

}

?>